sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
], function(Controller, MessageToast, Filter, FilterOperator) {
	"use strict";
	return Controller.extend("sap.ui.demo.listing.controller.List", {
		onPress: function(oEvent) {
			var oItem = oEvent.getSource();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("detail", {
				customerPath: oItem.getBindingContext("northwind").getPath().substr(1)
			});
		},
		
		onOpenDialog: function(oEvent) {
			var oView = this.getView();
	        var oDialog = oView.byId("dialog");
	        
	        if (!oDialog) {
	            oDialog = sap.ui.xmlfragment(oView.getId(), "sap.ui.demo.listing.view.Dialog", this);
	            oDialog.setModel(oEvent.getSource().getBindingContext("northwind"));
	            oView.addDependent(oDialog);
	        }

	        oDialog.open();
		},
		
		onAcceptDialog: function() {
			this.getView().byId("dialog").close();
			MessageToast.show(this.getView().getModel("i18n").getResourceBundle().getText("dialogToast"));
		},
		
		onRejectDialog: function() {
			this.getView().byId("dialog").close();
		},
		
		onSearch: function(oEvent) {
			var aFilter = [];
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				aFilter.push(new Filter({
					filters: [
						new Filter("CompanyName", FilterOperator.Contains, sQuery),
						new Filter("CustomerID", FilterOperator.Contains, sQuery),
						new Filter("ContactName", FilterOperator.Contains, sQuery),
						new Filter("ContactTitle", FilterOperator.Contains, sQuery),
						new Filter("Address", FilterOperator.Contains, sQuery),
						new Filter("PostalCode", FilterOperator.Contains, sQuery),
						new Filter("City", FilterOperator.Contains, sQuery),
						new Filter("Country", FilterOperator.Contains, sQuery),
						new Filter("Phone", FilterOperator.Contains, sQuery),
						new Filter("Fax", FilterOperator.Contains, sQuery)
					],
					and: false
				}));
			}

			var oTable = this.byId("customersTable");
			var oBinding = oTable.getBinding("items");
			oBinding.filter(aFilter);
		}
	});
});